Katie presented:

"Why Work?: Older Women's Identity, Certainty, and Motivations for Continued Work"
___
Nicky presented: 

"Women and Retirement: Context, and Psychosocial Resources"
___
Vivian presented:

"For Self or Others?: A Qualitative Study of Older Chinese and American Women's Generativity"
