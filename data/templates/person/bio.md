Text for a biography can go here, and takes on styling using [MarkDown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) syntax.
You can find this file in **data/people/present/NAME-OF-MEMBER-HERE/bio.md**, and can open it in a basic text editor, like Notepad on Windows or TextEdit on OSX.
Don't forget to replace **data/people/present/NAME-OF-MEMBER-HERE/cv.pdf** with an appropriate cv PDF file, and **data/people/present/NAME-OF-MEMBER-HERE/photo.jpg** with a JPG photo for this new lab member!
