export default {
  year: 2019, // Make sure this is the correct year
  title: 'Put the name of the conference here',
  subtitle: 'Symposium Presentation',
  caption: 'Caption the conference photo',
};
