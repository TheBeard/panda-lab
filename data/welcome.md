# Welcome to the Personality and Development in Adulthood (PanDA) lab at Wilfrid Laurier University!

Our research examines a wide variety of factors associated with adult development, with a focus on personality in its many forms.

We use qualitative and quantitative methods to study transitions during the adult lifespan, such as retirement, and the ways in which individual factors (personality, social roles, social networks, gender) are associated with adapting to aging.

## Research

Current research projects address questions such as: how do men and women negotiate potentially new identities as they age?
How does personality relate to the feelings of regret?
What is the impact of a telephone program for socially-isolated older adults?
What is the role of stress – both one’s own and that of others’ – for older adults’ emotional well-being?
How do younger and older adults deal with on-time or off-time bereavement?
Given that models of aging have not tended to fully capture women’s experiences, what specific aging-related issues do women identify as important? How can we use our data to develop more inclusive models of aging? 


