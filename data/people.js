import get from 'lodash.get';

export const heading = {
  image: require('./images/lab.jpg'),
  caption: require('./labPhotoCaption.md'),
};

const personOrder = [
  'Associate Professor',
  'PhD Student',
  'MA Student',
  'Research Assistant',
  '',
  undefined,
  undefined,
];

const getOrder = position => {
  const index = personOrder.indexOf(position)
  if (index === -1) {
    return 5;
  }
  return index;
}

const sortPeople = (a, b) => {
  const positionDiff = getOrder(a.position) - getOrder(b.position);
  if (positionDiff !== 0) {
    return positionDiff;
  }
  const imageDiff = Number(!!b.image) - Number(!!a.image);
  if (imageDiff !== 0) {
    return imageDiff;
  }

  return a.name.localeCompare(b.name);
};

const data = {
  present: {
    people: require('./people/present/*/index.json'),
    images: require('./people/present/*/photo.jpg'),
    pdfs: require('./people/present/*/cv.pdf'),
    docs: require('./people/present/*/bio.md'),
  },
  past: {
    people: require('./people/past/*/index.json'),
    images: require('./people/past/*/photo.jpg'),
    pdfs: require('./people/past/*/cv.pdf'),
    docs: require('./people/past/*/bio.md'),
  },
};

const loadPerson = dataSet => name => {
  const fullData = dataSet === 'present';
  const base = data[dataSet].people[name];
  return {
    ...base,
    text: fullData ? get(data, `${dataSet}.docs.${name}`, '') : '',
    image: fullData ? get(data, `${dataSet}.images.${name}`) : '',
    cv: base.cv || get(data, `${dataSet}.pdfs.${name}`),
  };
};

export const present = Object.keys(data.present.people).map(loadPerson('present')).sort(sortPeople);
export const past = Object.keys(data.past.people).map(loadPerson('past')).sort(sortPeople);
