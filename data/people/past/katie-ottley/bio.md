I am a Masters student, with an interest in life span development.
I completed an Honours degree in Psychology at the University of Regina.
My honours project investigated the impact of nature exposure on psychological wellbeing under the supervision of Dr. Phillip Sevigny.
My current research interests include the impact of normative and non-normative life transitions (especially bereavement) on identity, and perceived social support in adulthood.

