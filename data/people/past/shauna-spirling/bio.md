I am a second year Masters student in Developmental Psychology, working under the supervision of Dr. Nicky Newton.
My current research utilizes Erik Erikson’s theoretical framework of aging in order to examine psychosocial development within the context of older adulthood.
More specifically, my primary focus is to explore the relationship between generativity, ego integrity, the experience of regret, and well-being among retirees.
It is my goal to contribute to the field of adult aging and development in a way that sheds light on facilitative factors of successful aging and better psychological health in retirement.
