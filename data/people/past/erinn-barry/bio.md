I previously graduated with an honours BA in Psychology (Concentration: Research Specialist), and a minor in Philosophy, from Wilfrid Laurier University (WLU) in 2018.
Under the supervision of Dr. Newton, I am currently pursuing an MA in Developmental Psychology at WLU.
My research interests are wide ranging, but currently include: gender differences in the symptom presentation of high-functioning autism (i.e., the female autism phenotype), psychometrics, and personality.
