Nicky Newton is an Assistant Professor at Wilfrid Laurier University in Waterloo, Canada.
Previously, she was a Research Fellow at the Survey Research Center at the University of Michigan (2014-2015), where she also received her Ph.D. in Personality and Social Contexts Psychology in 2011.
In between, she was an Assistant Professor at Youngstown University, and Assistant Research Professor at Northwestern University.
Nicky’s research focuses on the relationships between personality, social roles, social support, health, and well-being across adulthood.
Most recent projects include retirement experiences and expectations, a multi-faceted study of older women’s lives, and the commonalities and differences in various life transitions across adulthood.

