Taylor's Master's thesis reports on her analysis of a longitudinal cohort dataset on women’s health and well-being through their adulthood.
Specifically, changes in subjective vitality, life satisfaction, and psychosocial growth, all in relation to life-changing events such as retirement and bereavement and everyday life such as occupational/recreational/leisurely time use.
At heart, she is a methodologist and an advocate for knowledge synthesis and mobilization, and she maintains a secondary line of research on public health and education.
Overall, her research focuses on understanding and promoting the well-being of different populations through implementation studies, qualitative and quantitative methods, environmental scans, survey data, and secondary data analysis.
She plans to begin her PhD in fall of 2020 on the role of leisure in psychosocial adjustment to a cancer diagnosis.
