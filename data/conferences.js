const list = require('./conferences/*/index.json');
const photos = require('./conferences/*/photo.jpg');
const descriptions = require('./conferences/*/description.md');

export const conferences = Object.keys(list)
  .map((name) => ({
    ...list[name],
    image: photos[name],
    description: descriptions[name],
  }))
  .sort((a, b) => b.year - a.year);
