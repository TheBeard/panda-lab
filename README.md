# Wilfrid Laurier University PanDA Lab website

## What's this

This is a guide for Nicky and students to get set up to update the website.

### Where can I see what's done?

The latest version of the site will usually be [here](https://pandalab.ca/) or [here as a backup](https://panda-lab-76257.firebaseapp.com/).

## How do I update it?

The general flow of updating the website will be:

1. Get a copy of the website on your local computer from Bitbucket
2. Run the server (it will open your web browser for you and show you the local copy of the website)
4. Edit Files
5. See live changes

And when you're satisfied with your changes:

1. Add changes to git, and commit them (we'll go over that coming up)
2. Push changes to Bitbucket
3. Wait a couple of minutes (you might get an email when it's done)
4. Changes should now be live on https://pandalab.ca/ (and you should double check in case something went wrong).

## Get added to the Bitbucket Repository

To update the website, we are using Bitbuck to handle versioning and deploying the website.

Nicky should read through [the documentation to add a student](./NICKY_INVITE_STUDENT.md).

Once you're invited, **you will need to follow the [student one time setup instructions](./STUDENT_ONE_TIME_SETUP.md) if you are using your personal computer to update the website.**
Nicky may set up the lab's OSX computer, in which case you may not need to follow those steps, check with her first if you are using the lab computer.

## Run the site locally

Running the site locally means we run a small server on your computer that will let you access a local copy of the website, where it's safe to make changes without disrupting the real website on https://pandalab.ca/ .

 1. In your terminal, type `cd ~/panda-lab`, and press Enter.
 2. In your terminal, type `yarn start`, and press Enter. This will run the server, and open your web browser to a local copy of the panda-lab website.
 3. When you're done running the web server, click on your terminal and press `Ctrl+c` to stop the server from running.

## Make a change

**IMPORTANT:** Before you update anything, do the following:

 1. Open Github Desktop
 2. There are three buttons at the top of the application, "Current Repository", "Current Branch", and one last button, which will either say "Fetch Origin", "Pull", or "Push".
 3. If it says "Fetch Origin" or "Pull", click the button. This will bring in any changes that myself or a member of the lab has made.

### Update the lab photo

 1. Open Github Desktop
 2. In Github Desktop, click the "Repository" menu
 3. In the "Repository" menu, click "Show in your File Manager"
 4. In Finder (Apple) or Explorer (Non-Apple), double-click on the `data` folder.
 5. In Finder (Apple) or Explorer (Non-Apple), double-click on the `images` folder.
 6. In Finder (Apple) or Explorer (Non-Apple), paste the new image in this folder. It must be a JPEG file named `lab.jpg`.
 7. In Github Desktop, click the "Repository" menu
 8. In the "Repository" menu, click "Open in Terminal" (OSX) or "Open in Git Bash" (Windows).
 7. In your terminal, type `bash .nvmrc | nvm use && yarn start` to run the server, and see your changes. If your server was already running, you may need to restart it by clicking on your terminal, and pressing `Ctrl+c`, then typing `yarn start` again. The web server we're using does not like it when we move or delete folders (but it's okay with us creating new ones).
 8. Observe the changes in your web browser.
 9. In Finder (Apple) or Explorer (Non-Apple), navigate up one directory, back to `data`.
 10. Open `labPhotoCaption.md` in TextEdit (do **not** use something like Microsoft Word or other word processing apps, they will do horrible things to these files). This is a markdown file, you can see how to format the text by reading [this handy webpage](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).
 11. Update the text to reflect he people and ordering in the new photo
 12. Observe the changes in your web browser.


### Move a person from the present list to the past list

These instructions can also be reversed if you need to move someone from past into present.

 1. Open Github Desktop
 2. In Github Desktop, click the "Repository" menu
 3. In the "Repository" menu, click "Show in your File Manager"
 4. In Finder (Apple) or Explorer (Non-Apple), double-click on the `data` folder.
 5. In Finder (Apple) or Explorer (Non-Apple), double-click on the `people` folder.
 6. In Finder (Apple) or Explorer (Non-Apple), double click on the `present` folder.
 7. Press `Command+n` to open a new Finder window.
 8. Follow steps 2 to 5 (it may put you in the present folder by default, in which case, navigate up one folder to `people`)
 9. In the first Finder window (`present`), drag the folder of the person you want to move into the second Finder window (`past`)
 10. In your terminal, type `cd ~/panda-lab && nvm use && yarn start` to run the server, and see your changes. If your server was already running, you may need to restart it by clicking on your terminal, and pressing `Ctrl+c`, then typing `yarn start` again. The web server we're using does not like it when we move or delete folders (but it's okay with us creating new ones).
 11. Observe the changes in your web browser.

### Add a person to the lab

 1. Open Github Desktop
 2. In Github Desktop, click the "Repository" menu
 3. In the "Repository" menu, click "Show in your File Manager"
 4. In Finder (Apple) or Explorer (Non-Apple), double-click on the `data` folder.
 5. In Finder (Apple) or Explorer (Non-Apple), double-click on the `templates` folder.
 6. In Finder (Apple) or Explorer (Non-Apple), click on the `person` folder, and press `Command+c` to copy it.
 7. In Finder (Apple) or Explorer (Non-Apple), navigate up one directory, back to the `data` folder.
 8. In Finder (Apple) or Explorer (Non-Apple), double-click on the `people` folder.
 9. In Finder (Apple) or Explorer (Non-Apple), double-click on the `present` folder.
 10. In Finder (Apple) or Explorer (Non-Apple), click somewhere in the folder, and press `Command+v` to paste in the template folder.
 11. In Finder (Apple) or Explorer (Non-Apple), two-finger click on the new folder, and select the "Rename" option in the menu.
 12. Type in the new members name as the folder name, but all lower case, and replace any spaces with dashes. When done, press Enter.
 13. In Finder (Apple) or Explorer (Non-Apple), double click on the newly copied and named folder.
 14. If you have a photo of the lab member, convert it to a jpeg file, copy it to this folder, and name it `photo.jpg`.
 15. If you have a CV pdf from the lab member, copy it to this folder, and name it `cv.pdf`.
 16. Open `index.json` in TextEdit (do **not** use something like Microsoft Word or other word processing apps, they will do horrible things to these files).
 17. In this file, update their name, position (which should be one of `PhD Student`, `MA Student`, or `Research Assistant`, spelling counts on this one), and you can optionally add links to their linkedIn and researchgate profiles, as well as a website if they have one.
 18. If they have a biography that Nicky has approved, open `bio.md`, and replace the contents with the text you have been supplied. This is a markdown file, you can see how to format the text by reading [this handy webpage](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).
 19. In your terminal, type `cd ~/panda-lab && nvm use && yarn start` to run the server, and see your changes. If your server was already running, you may need to restart it by clicking on your terminal, and pressing `Ctrl+c`, then typing `yarn start` again. The web server we're using does not like it when we move or delete folders (but it's okay with us creating new ones).
 20. Observe the changes in your web browser.

### Add a conference

 1. Open Github Desktop
 2. In Github Desktop, click the "Repository" menu
 3. In the "Repository" menu, click "Show in your File Manager"
 4. In Finder (Apple) or Explorer (Non-Apple), double-click on the `data` folder.
 5. In Finder (Apple) or Explorer (Non-Apple), double-click on the `templates` folder.
 6. In Finder (Apple) or Explorer (Non-Apple), click on the `conference` folder, and press `Command+c` to copy it.
 7. In Finder (Apple) or Explorer (Non-Apple), navigate up one directory, back to the `data` folder.
 8. In Finder (Apple) or Explorer (Non-Apple), double-click on the `conferences` folder.
 9. In Finder (Apple) or Explorer (Non-Apple), click somewhere in the folder, and press `Command+v` to paste in the template folder.
 10. In Finder (Apple) or Explorer (Non-Apple), two-finger click on the new folder, and select the "Rename" option in the menu.
 11. Type in the `year-name-of-conference` where `name-of-conference` is the name of the conference, in all lower case, and replace any spaces with dashes. When done, press Enter.
 12. In Finder (Apple) or Explorer (Non-Apple), double click on the newly copied and named folder.
 13. If you have a photo of the conference, convert it to a jpeg file, copy it to this folder, and name it `photo.jpg`.
 14. Open `index.json` in TextEdit (do **not** use something like Microsoft Word or other word processing apps, they will do horrible things to these files).
 15. In this file, update the year, proper name, subtitle, and photo caption to proper values.
 16. Open `description.md` in TextEdit (do **not** use something like Microsoft Word or other word processing apps, they will do horrible things to these files).
 17. In this file, add details about the conference. This is a markdown file, you can see how to format the text by reading [this handy webpage](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).
 18. In your terminal, type `cd ~/panda-lab && nvm use && yarn start` to run the server, and see your changes. If your server was already running, you may need to restart it by clicking on your terminal, and pressing `Ctrl+c`, then typing `yarn start` again. The web server we're using does not like it when we move or delete folders (but it's okay with us creating new ones).
 19. Observe the changes in your web browser.

### Add a new publication

 1. Open Github Desktop
 2. In Github Desktop, click the "Repository" menu
 3. In the "Repository" menu, click "Show in your File Manager"
 4. In Finder (Apple) or Explorer (Non-Apple), double-click on the `data` folder.
 5. In Finder (Apple) or Explorer (Non-Apple), double-click on the `templates` folder.
 6. Open `publications.json` in TextEdit.
 7. Go to the last publication in the list, and add a new line between itself and the last line with the `]` bracket.
 8. Add two double-quotes, and paste the publication information between those quotes. **IMPORTANT:** If any part of the text you pasted in has double quotes, you will need to find those manually and add a backslash (`\`) before them, so they will become `\"`.
 9. In your terminal, type `cd ~/panda-lab && nvm use && yarn start` to run the server, and see your changes. If your server was already running, you may need to restart it by clicking on your terminal, and pressing `Ctrl+c`, then typing `yarn start` again. The web server we're using does not like it when we move or delete folders (but it's okay with us creating new ones).
 10. Observe the changes in your web browser.

### Add research

 1. Open Github Desktop
 2. In Github Desktop, click the "Repository" menu
 3. In the "Repository" menu, click "Show in your File Manager"
 4. In Finder (Apple) or Explorer (Non-Apple), double-click on the `data` folder.
 5. Open `research.md` in TextEdit (do **not** use something like Microsoft Word or other word processing apps, they will do horrible things to these files).
 6. Add a new line, and type ` - **TITLE HERE**: description here.` where `TITLE HERE` is the proper title, and `description here` is a summary of the research.
 7. In your terminal, type `cd ~/panda-lab && nvm use && yarn start` to run the server, and see your changes. If your server was already running, you may need to restart it by clicking on your terminal, and pressing `Ctrl+c`, then typing `yarn start` again. The web server we're using does not like it when we move or delete folders (but it's okay with us creating new ones).
 8. Observe the changes in your web browser.

### Change the text on the landing page

 1. Open Github Desktop
 2. In Github Desktop, click the "Repository" menu
 3. In the "Repository" menu, click "Show in your File Manager"
 4. In Finder (Apple) or Explorer (Non-Apple), double-click on the `data` folder.
 5. Open `welcome.md` in TextEdit (do **not** use something like Microsoft Word or other word processing apps, they will do horrible things to these files).
 6. In your terminal, type `cd ~/panda-lab && nvm use && yarn start` to run the server, and see your changes. If your server was already running, you may need to restart it by clicking on your terminal, and pressing `Ctrl+c`, then typing `yarn start` again. The web server we're using does not like it when we move or delete folders (but it's okay with us creating new ones).
 7. Observe the changes in your web browser.

### Change images on the landing page carousel

 1. Open Github Desktop
 2. In Github Desktop, click the "Repository" menu
 3. In the "Repository" menu, click "Show in your File Manager"
 4. In Finder (Apple) or Explorer (Non-Apple), double-click on the `data` folder.
 5. In Finder (Apple) or Explorer (Non-Apple), double-click on the `images` folder.
 6. In Finder (Apple) or Explorer (Non-Apple), double-click on the `welcome-slides` folder.
 7. Add `*.jpg` images here. Make sure they are landscape (wide), and not portrait (tall).
 8. In your terminal, type `cd ~/panda-lab && nvm use && yarn start` to run the server, and see your changes. If your server was already running, you may need to restart it by clicking on your terminal, and pressing `Ctrl+c`, then typing `yarn start` again. The web server we're using does not like it when we move or delete folders (but it's okay with us creating new ones).
 9. Observe the changes in your web browser.

## Upload your change to the website

Once you've made your change, open "Github Desktop"

 1. Make sure the "Current repository" on the top-left of the application reads "panda-lab"
 2. Ensure all the files on the left side are checked.
 3. On the bottom left, there are two text input fields. The top one is a required short summary, and the bottom is an optional description. Make sure these are filled out properly, with meaningful text.
 4. Click the blue "Commit to master" button
 5. On the top middle, click the black "Push origin" button.
 6. Wait a couple of minutes.
 7. https://pandalab.ca should be updated

If something went wrong, you will likely recieve an email form Bitbuck saying the pipeline failed.
This is likely because a `*.json` file that was changed was improperly formatted.
If you're not sure what went wrong, talk to Nicky, and she will contact me.
