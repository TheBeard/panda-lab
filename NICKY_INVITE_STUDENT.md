Nicky, you'll need to follow these steps every time a new student will use their own computer to update the website.
You could optionally follow through the [student one time setup instructions](./STUDENT_ONE_TIME_SETUP.md) once on the lab OSX computer, and have students only use that to update the website.

 1. Open a web browser, and go to https://bitbucket.org/TheBeard/panda-lab/
 2. On the left page navigation, click "Settings"
 3. In the Settings navigation (also on the left, right of the navigation used above)
 4. Click on "User and group access"
 5. Under the "Users" section, click on the input "Add a user by their name or email address"
 6. Enter the email address of the person you want to add, preferably their laurier email address.
 7. Immediately to the right of the input area, there is some text that says "Read" with a downward arrow, click that, and change it to "Write"
 8. Click the "Add" link on the right of the input area on the same line.

