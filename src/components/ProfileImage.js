import { h } from 'hyperapp';

import './ProfileImage.styl';

export const ProfileImage = ({ src, size, alt }) => h(
  'figure',
  {
    class: 'profile-image',
    style: {
      width: size,
      height: size,
      backgroundImage: `url(${src})`,
    },
  },
  [
    h('figcaption', { class: 'screen-reader-only' }, alt),
  ]
);
