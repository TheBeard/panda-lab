import { h } from 'hyperapp';
import { ProfileImage } from './ProfileImage';

import './Person.styl';

const linkedInImage = require('../assets/In-2C-128px-TM.png');
const researchgateImage = require('../assets/RG_square_green.jpg');

const footerKeys = ['linkedIn', 'cv', 'website', 'researchgate'];
const hasFooterContent = (props) => footerKeys.some(k => Boolean(props[k]));

export const Person = (props) => {
  const { image, name, position, text, linkedIn, cv, website, researchgate } = props;

  return h(
    'section',
    {
      class: 'person',
    },
    [
      h(
        'header',
        {
          class: ['person-header'].concat(text ? 'person-header--with-content' : []).join(' '),
        },
        [
          image && h(
            ProfileImage,
            {
              size: '128px',
              src: image,
              alt: `A picture of ${name}`,
            }
          ),
          h(position ? 'h3' : 'div', { class: 'person-name' }, name),
          h('span', { class: 'person-position' }, position),
        ]
      ),
      h('article', {
        class: ['person-text'].concat(text && hasFooterContent(props) ? 'person-text--with-footer' : []).join(' '),
        oncreate: e => e.innerHTML = text,
      }),
      hasFooterContent(props) && h('footer', { class: 'person-footer' }, [
        linkedIn && h('a', { href: linkedIn, class: 'person-footer-link', target: '__blank', rel: 'noopener noreferrer' }, [
          h('img', { src: linkedInImage, alt: `Go to ${name}'s LinkedIn page.`, class: 'person-linkedin-image' }),
        ]),
        researchgate && h('a', { href: researchgate, class: 'person-footer-link', target: '__blank', rel: 'noopener noreferrer' }, [
          h('img', { src: researchgateImage, alt: `Go to ${name}'s ResearchGate page.`, class: 'person-linkedin-image' }),
        ]),
        h('div', { style: { flexGrow: 1 } }),
        cv && h('a', { href: cv, class: 'person-footer-link', target: '__blank', download: `${name} - CV.pdf` }, 'CV'),
        website && h('a', { href: website, class: 'person-footer-link', target: '__blank' }, 'Website'),
      ]),
    ]
  );
};
