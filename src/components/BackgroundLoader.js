import { h } from 'hyperapp';

import './BackgroundLoader.styl';

const showLoader = (props) => {
  if (props.percentage < 1) return true;
  return !props.hideAt100;
};

export const BackgroundLoader = (props, children) => h(
  'div',
  null,
  [
    h(
      'div',
      {
        class: 'background-loader',
        style: {
          width: `${Math.ceil((props.percentage || 0) * 100)}%`,
          display: showLoader(props) ? 'block' : 'none',
        },
      }
    ),
  ].concat(children)
);
