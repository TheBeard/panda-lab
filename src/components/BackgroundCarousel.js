import { h } from 'hyperapp';
import './BackgroundCarousel.styl';

export const state = {
  currentImage: null,
};

export const actions = {
  setImage: currentImage => ({ currentImage }),
};

export const BackgroundCarousel = (props) => {
  return h(
    'div',
    {
      class: ['background-carousel', props.class].filter(Boolean).join(' '),
      style: {
        backgroundImage: `url(${props.currentImage})`,
      },
    }
  );
};
