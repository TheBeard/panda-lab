import { h } from 'hyperapp';
import { Link } from '@hyperapp/router';
import './Layout.styl';

const laurierLogo = require('../assets/specialty-purple-leaf-digital.jpg');

export const initialState = {
  collapsibleOpen: false,
};

export const actions = {
  toggleCollapsibleOpen: () => state => ({ collapsibleOpen: !state.collapsibleOpen }),
};

const collapsibleModifier = collapsibleOpen => collapsibleOpen
  ? 'collapsible-content--open'
  : '';

export const Header = ({ collapsibleOpen, toggleCollapsibleOpen }) =>
  h('nav', { class: 'header' }, [
    h('div', { class: 'header-container' }, [
      h('div', { class: 'header-container__left' }, [
        h('img', { class: 'header-logo', src: laurierLogo, alt: 'Wilfrid Laurier University, Inspiring Lives logo' }, []),
        h('span', { class: 'header-title' }, [
          h('span', { class: 'header-title__lab' }, 'PanDA Lab'),
        ]),
        h('div', { class: 'spacer' }),
      ]),
      h('div', { class: 'header-container__right collapsible' }, [
        h('button', { class: 'collapsible-mobile-button', onclick: toggleCollapsibleOpen }, [
          h('hr'),
          h('hr'),
          h('hr'),
          h('span', { class: 'screen-reader-only' }, 'Expand Navigation'),
        ]),
        h('ul', { class: ['collapsible-content', collapsibleModifier(collapsibleOpen)].join(' ') }, [
          h('li', { class: 'item' }, [h(Link, { to: '/' }, 'Welcome')]),
          h('li', { class: 'item' }, [h(Link, { to: '/people' }, 'People')]),
          h('li', { class: 'item' }, [h(Link, { to: '/research' }, 'Research')]),
          h('li', { class: 'item' }, [h(Link, { to: '/conferences' }, 'Conferences')]),
          h('li', { class: 'item' }, [h('a', { href: 'mailto:nnewton@wlu.ca' }, 'Contact Us')]),
        ]),
      ]),
    ]),
  ]);

export const Footer = () =>
  h('footer', { class: 'footer' }, [
    h('section', { class: 'footer-container' }, [
      h('ul', { class: 'footer-list' }, [
        h('li', {}, h(Link, { class: 'footer-link', to: '/' }, 'Welcome')),
        h('li', {}, h(Link, { class: 'footer-link', to: '/people' }, 'People')),
        h('li', {}, h(Link, { class: 'footer-link', to: '/publications' }, 'Publications')),
      ]),
      h('ul', { class: 'footer-list footer-info' }, [
        h('li', {}, 'If you would like more information about our research, please contact Nicola Newton, PhD'),
        h('li', {}, h('a', { href: 'mailto:nnewton@wlu.ca', class: 'footer-link' }, 'nnewton@wlu.ca')),
        h('li', {}, [
          h('div', null, 'Wilfrid Laurier University'),
          h('div', null, '75 University Avenue West'),
          h('div', null, 'Waterloo, Ontario'),
          h('div', null, 'N2L 3C5'),
        ]),
      ]),
    ]),
  ]);

export const Layout = (props, children) => {
  const { toggleCollapsibleOpen } = props.actions;
  const { collapsibleOpen } = props.state;

  return (
    h('div', { class: 'layout' }, [
      Header({ collapsibleOpen, toggleCollapsibleOpen }),
      h('article', { role: 'main', class: 'layout-content' }, children),
      Footer(),
    ])
  );
};
