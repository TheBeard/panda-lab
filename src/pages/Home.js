import { h } from 'hyperapp';

import { Layout } from '../components/Layout';
import { BackgroundLoader } from '../components/BackgroundLoader';
import * as Carousel from '../components/BackgroundCarousel';

import welcomeMessage from '../../data/welcome.md';

import './Home.styl';

const welcomeSlides = Object.values(require('../../data/images/welcome-slides/*.jpg'));

const IMAGE_DELAY = 10000;

export const initialState = {
  images: welcomeSlides.map(src => ({ src, img: null })),
  imageTimeout: null,
  backgroundCarousel: Carousel.state,
  welcomeMessage,
};
export const actions = {
  loadImages: () => (state, actions) => {
    const allDone = state.images.every(({ img }) => img && img.complete);
    if (allDone) {
      actions.goToNextBackgroundImage();
      return {};
    }

    return {
      images: state.images
        .map(image => {
          if (image.img) return image;

          const img = new Image();

          img.onload = () => {
            actions.setBackgroundIfNeeded(image.src);
          };
          img.onerror = () => {
            console.warn('Unable to load image', image.src); // eslint-disable-line no-console
          };
          img.src = image.src;

          return Object.assign({ img }, image);
        }),
    };
  },

  setBackgroundIfNeeded: backgroundImageSrc => (state, actions) => {
    if (!state.backgroundCarousel.currentImage) {
      actions.setBackgroundImageSrc(backgroundImageSrc);
    }
  },

  setBackgroundImageSrc: backgroundImageSrc => (state, actions) => {
    if (state.imageTimeout) clearTimeout(state.imageTimeout);
    const imageTimeout = setTimeout(() => {
      actions.goToNextBackgroundImage();
    }, IMAGE_DELAY);

    actions.backgroundCarousel.setImage(backgroundImageSrc);
    return {
      imageTimeout,
    };
  },

  backgroundCarousel: Carousel.actions,

  goToNextBackgroundImage: () => (state, actions) => {
    const idx = state.images.findIndex(i => i.src === state.backgroundCarousel.currentImage);
    const next = (idx + 1) % state.images.length;
    actions.setBackgroundImageSrc(state.images[next].src);
    return null;
  },

  setWelcomeMessage: target => (state) => {
    target.innerHTML = state.welcomeMessage;
    return null;
  },

  clearImageTimeout: () => state => {
    if (state.imageTimeout) {
      clearTimeout(state.imageTimeout);
    }
    return {
      imageTimeout: null,
    };
  },
};

const viewContent = ({ state, actions }) => {
  const loadedImages = state.images
    .filter(img => img.state !== 'loading')
    .length;

  return h(BackgroundLoader, { percentage: loadedImages / state.images.length, hideAt100: true }, [
    h(
      Layout,
      { state, actions },
      [
        h(Carousel.BackgroundCarousel, Object.assign({
          class: 'home-carousel',
          src: state.backgroundImageSrc,
        }, state.backgroundCarousel, actions.backgroundCarousel)),
        h('div', { class: 'home' }, [
          h('div', { oncreate: actions.setWelcomeMessage }),
        ]),
      ]
    ),
  ]);
};

export const view = (props) =>
  h(
    'div',
    {
      oncreate: props.actions.loadImages,
      ondestroy: props.actions.clearImageTimeout,
    },
    [
      viewContent(props),
    ]
  );
