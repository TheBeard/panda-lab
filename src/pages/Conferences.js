import { h } from 'hyperapp';

import { Layout } from '../components/Layout';

import * as ConferencesData from '../../data/conferences';

import './Conferences.styl';

export const initialState = {
  conferences: ConferencesData.conferences,
};
export const actions = {};

const linkNameForConference = ({ title }) => title.toLowerCase().replace(/\W+/g, '-');

const Conference = (props) => h(
  'article',
  null,
  [
    h('h2', null, [
      h('a', { class: 'anchor', name: linkNameForConference(props), href: '#' + linkNameForConference(props) }, [
        props.title,
      ]),
    ]),
    h('h3', null, props.subtitle),
    h('div', { class: 'body' }, [
      h('figure', { class: 'image-box' }, [
        h('img', { src: props.image, alt: props.caption }),
        h('figcaption', null, props.caption)
      ]),
      h('section', { oncreate: e => e.innerHTML = props.description }),
    ]),
  ],
);

/*

<article>
  <h1>Name of conference</h1>
  <h2>Name of Event in Conference</h2>
  <img />
  <caption></caption>
  <p>jgfdfkjgbkjdfngkjndfkjngfdkjnfgdnkjgfdnkj</p>
</article>

*/


export const view = (props) => h(
  Layout,
  props,
  [
    h('div', { class: 'conferences' }, props.state.conferences.map(data => h(Conference, data))),
  ]
);
