import { h } from 'hyperapp';

import { Layout } from '../components/Layout';
import { Person } from '../components/Person';

import * as PeopleData from '../../data/people';

import './People.styl';

export const initialState = {
  heading: PeopleData.heading,
  past: PeopleData.past,
  present: PeopleData.present,
};
export const actions = {
  injectMarkdown: e => state => e.innerHTML = state.heading.caption,
};

const PeopleSection = ({ title }, children) => h(
  'section',
  {
    class: 'people-group',
  },
  [
    h('h2', null, title),
    children,
  ]
);

const PeopleList = ({ group }) => h(
  'ul',
  { class: 'people-list' },
  group.map((personProps) => h(
    'li',
    null,
    [
      h(Person, personProps),
    ]
  ))
);

const PeopleGroup = ({ title, group }) => group.length > 0 && PeopleSection({ title }, PeopleList({ group }));
const PeopleMultiGroup = ({ title, groups }) => PeopleSection({ title }, groups.map(group => group.length > 0 && PeopleList({ group })));

export const view = (props) => {
  const presentGroups = [
    props.state.present.filter(p => p.position !== 'Research Assistant'),
    props.state.present.filter(p => p.position === 'Research Assistant'),
  ];

  return h(
    Layout,
    props,
    [
      h('div', {
        class: 'people',
      }, [
        h('h1', null, 'People in the Lab'),

        h('figure', { class: 'people__lab-photo image-box' }, [
          h('img', {
            class: 'people__lab-photo-image',
            src: props.state.heading.image,
            alt: props.state.heading.caption,
          }),
          h('figcaption', { oncreate: e => e.innerHTML = props.state.heading.caption }),
        ]),

        h(PeopleMultiGroup, { title: 'Present', groups: presentGroups }),

        h(PeopleGroup, { title: 'Past', group: props.state.past }),

      ]),
    ]
  );
};
