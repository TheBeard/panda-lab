import { h } from 'hyperapp';

import { Layout } from '../components/Layout';

import './Research.styl';
import researchHtml from '../../data/research.md';

export const initialState = {
  publications: require('../../data/publications.json'),
};
export const actions = {
  injectMarkdown: e => e.innerHTML = researchHtml,
};

export const view = props => h(
  Layout,
  props,
  [
    h('h1', null, 'Research'),
    h('div', { oncreate: props.actions.injectMarkdown, class: 'publications-markdown' }),
    h('h2', null, 'Publications'),
    h(
      'ul',
      {
        class: 'publications',
      },
      props.state.publications.map(pub => h(
        'li',
        {
          class: 'publications-item',
        },
        [
          pub,
        ]
      ))
    ),
  ]
);
