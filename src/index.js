import { app, h } from 'hyperapp';
import { location, Route } from '@hyperapp/router';

import * as Layout from './components/Layout';

import * as Home from './pages/Home';
import * as People from './pages/People';
import * as Research from './pages/Research';
import * as Conferences from './pages/Conferences';

const initialState = {
  layout: Layout.initialState,
  home: Home.initialState,
  people: People.initialState,
  research: Research.initialState,
  conferences: Conferences.initialState,
  location: location.state,
};

const initialActions = {
  layout: Layout.actions,
  home: Home.actions,
  people: People.actions,
  research: Research.actions,
  conferences: Conferences.actions,
  location: location.actions,
};

const propsForView = (state, actions, viewKey) =>
  ({
    state: Object.assign({}, state.layout, state[viewKey]),
    actions: Object.assign({}, actions.layout, actions[viewKey]),
  });

const view = (state, actions) =>
  h('div', null, [
    h(Route, { path: '/', render: () => h(Home.view, propsForView(state, actions, 'home')) }),
    h(Route, { path: '/people', render: () => h(People.view, propsForView(state, actions, 'people')) }),
    h(Route, { path: '/research', render: () => h(Research.view, propsForView(state, actions, 'research')) }),
    h(Route, { path: '/conferences', render: () => h(Conferences.view, propsForView(state, actions, 'conferences')) }),
  ]);

const interop = app(
  initialState,
  initialActions,
  view,
  document.getElementById('app')
);
location.subscribe(interop.location);
