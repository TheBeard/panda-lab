# One Time Instructions

Once you've been invited to the Bitbuck repository, you'll need to do these steps once so your computer will be able to run all the commands to update the website.
Most of these steps will be copy/paste text into a terminal, then press enter. Any time you see `text formatted like this`, it's probably something you should copy/paste.

## What you will need:

 - A Windows 10 Computer (if you're really not sure, you probably do if it's not an Apple computer).
 - Power adapter (this could be a little while, and a little intensive).
 - High-speed internet, we'll need to run a few updates on your computer and install packages from the internet. The wifi access at laurier may not be very fast, so prepare to wait a long time if that's the only internet access you have. Some of the downloads are quite heavy, so you may **prefer not** to connect your phone and use your data.
 - Install [Github Desktop](https://desktop.github.com/)
   - Should be a big purple button that says "Download for Windows (64bit)"

## Open the invite email

 1. The invite email should contain a link with text prompting you to join the bitbucket repository "panda-lab", click it.
 2. You will be prompted to set up an account with Bitbuck. Please use a secure password, one that you don't use with other accounts.

## Configure your computer to be able to update the website

First, we need to make an ssh key.

 1. Open up a terminal:
   - Click the start button (typically on the bottom left, it may say "Start" or just be a Windows logo)
   - Type "Git Bash", windows will start automatically searching, give it a second.
 2. Type `ssh-keygen -t rsa -b 4096 -C "your_email@example.com"` into your terminal **BUT** replace `your_email@example.com` with the email on your Bitbucket account. Once you've done that, press Enter.
 3. Your terminal will ask you were to save the ssh key file, just press Enter, you don't need to worry about that.
 4. Your terminal will ask you to put a password on your ssh key. This is optional (you can just press Enter twice to skip it), but I highly recommend you put a password on it. If you use a password, you will need it when you update the website.
 5. Type `ssh-add ~/.ssh/id_rsa` into your terminal, and press Enter.

Second, we need to install some things so you can run the website locally to make changes. You'll have to wait for each thing to finish before moving on to the next thing.

 1. [Click here](https://github.com/coreybutler/nvm-windows/releases/download/1.1.7/nvm-setup.zip) to download the nvm installer.
   - Once it's downloaded, double-click the zip file
   - Once it's extracted, double-click setup-nvm (the file name might end in .exe).
   - This will install a tool that will let you run the server.

## Configure your Bitbuck account

 1. In your terminal (same instructions before, Start Menu, "Git Bash"), type `clip < ~/.ssh/id_rsa.pub` and press Enter. This copies some text from your ssh key so Bitbuck can recognize you.
 2. Open https://bitbucket.org/dashboard/overview in your web browser (and log into Bitbucket if you aren't logged in).
 3. Click on your profile picture on the bottom-left.
 4. Click "View profile" in the popup menu.
 5. Click "Settings" on the left page navigation.
 6. Under the "Settings" navigation, find "SSH keys" and click on it.
 7. Click "Add key"
 8. Fill in the label with whatever you want, like "My Key"
 9. Click in the `Key` text area, and then paste (Command+V) your key (it should still be copied from step 1)
 10. Click the "Add key" button, and you're done.

From step 9, your key should look something like:

```text
ssh-rsa <a lot of garbage here, probably multiple lines of text> bobs@your-uncle.com
```

If it doesn't look like that, do step 1 again, and try pasting the text again in your web browser.

## Get a local copy of the website

 1. Open "Github Desktop" from your start menu.
 2. In Github Desktop, click the File menu
 3. In the File menu, click "Clone Repository"
 4. In the Clone Repository pop-up, click the "URL" tab
 5. In the "URL or username/repository" text input, type `git clone git@bitbucket.org:TheBeard/panda-lab.git`.
 6. Click the "Clone" button.
 7. Click the "Repository" menu.
 8. Click "Open in Git Bash" (this will open a terminal for you).
 3. In your terminal, type `cat .nvmrc | nvm install`, and press Enter. You'll only need to run this once, and it might take a minute, depending on how fast your internet is.
 4. In your terminal, type `cat .nvmrc | nvm use`, and press Enter.
 5. In your terminal, type `npm install -g yarn`, and press Enter. This will install a tool called `yarn`, which we'll use to install dependencies and run the server.
 6. In your terminal, type `yarn`, and press Enter. This will install the latest dependencies you need to run the server. It may occasionally change the `yarn.lock` file, and that's ok.
