# One Time Instructions

Once you've been invited to the Bitbuck repository, you'll need to do these steps once so your computer will be able to run all the commands to update the website.
Most of these steps will be copy/paste text into a terminal, then press enter. Any time you see `text formatted like this`, it's probably something you should copy/paste.

## What you will need:

 - A Mac OSX Computer (if you have something else, let Nicky know, and she'll tell me - I'll update instructions accordingly)
   - If you do **NOT** have an Apple computer, read through [the windows version of this guide](./STUDENT_ONE_TIME_SETUP.windows.md)!
 - Power adapter (this could be a little while, and a little intensive)
 - High-speed internet, we'll need to run a few updates on your computer and install packages from the internet. The wifi access at laurier may not be very fast, so prepare to wait a long time if that's the only internet access you have. Some of the downloads are quite heavy, so you may **prefer not** to connect your phone and use your data.

## Open the invite email

 1. The invite email should contain a link with text prompting you to join the bitbucket repository "panda-lab", click it.
 2. You will be prompted to set up an account with Bitbuck. Please use a secure password, one that you don't use with other accounts.

## Configure your computer to be able to update the website

First, we need to make an ssh key. I'll guide you through [this guide](https://help.github.com/en/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent).

 1. Open up a terminal (On OSX, `Command+Space`, then type `Terminal`, and press Enter)
 2. Type `ssh-keygen -t rsa -b 4096 -C "your_email@example.com"` into your terminal **BUT** replace `your_email@example.com` with the email on your Bitbucket account. Once you've done that, press Enter.
 3. Your terminal will ask you were to save the ssh key file, just press Enter, you don't need to worry about that.
 4. Your terminal will ask you to put a password on your ssh key. This is optional (you can just press Enter twice to skip it), but I highly recommend you put a password on it. If you use a password, you will need it when you update the website.
 5. Type `eval "$(ssh-agent -s)"` into your terminal, and press Enter.
 6. Type `ssh-add ~/.ssh/id_rsa` into your terminal, and press Enter.

Second, we need to install some things so you can run the website locally to make changes. You'll have to wait for each thing to finish before moving on to the next thing.

 1. Type `xcode-select --install` into your terminal, and press Enter. This could take a while, it's installing a bunch of developer tools, and _might_ require a computer restart. Sorry :(
 2. Type `curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash` into your terminal, and press Enter. This will install a tool that will let you run the server.
 3. Press `Command+q` to quit the terminal. Once it's closed, re-open it with `Command+Space`, type `Terminal` and press Enter. The previous step installs tools for our terminal, and we need to restart the terminal to be able to use them.

## Configure your Bitbuck account

 1. In your terminal, type `pbcopy < ~/.ssh/id_rsa.pub` and press Enter. This copies some text from your ssh key so Bitbuck can recognize you.
 2. Open https://bitbucket.org/dashboard/overview in your web browser (and log into Bitbucket if you aren't logged in).
 3. Click on your profile picture on the bottom-left.
 4. Click "View profile" in the popup menu.
 5. Click "Settings" on the left page navigation.
 6. Under the "Settings" navigation, find "SSH keys" and click on it.
 7. Click "Add key"
 8. Fill in the label with whatever you want, like "My Key"
 9. Click in the `Key` text area, and then paste (Command+V) your key (it should still be copied from step 1)
 10. Click the "Add key" button, and you're done.

From step 9, your key should look something like:

```text
ssh-rsa <a lot of garbage here, probably multiple lines of text> bobs@your-uncle.com
```

If it doesn't look like that, do step 1 again, and try pasting the text again in your web browser.

## Get a local copy of the website

 1. In your terminal, type `git clone git@bitbucket.org:TheBeard/panda-lab.git ~/panda-lab`, and press Enter. This will create a new directory in your home folder called `panda-lab`, you can see it in Finder if you want.
 2. In your terminal, type `cd panda-lab`, and press Enter. This changes the directory in the terminal to our newly created `panda-lab` folder.
 3. In your terminal, type `nvm install --latest-npm`, and press Enter. You'll only need to run this once, and it might take a minute, depending on how fast your internet is.
 4. In your terminal, type `nvm use`, and press Enter.
 5. In your terminal, type `npm install -g yarn`, and press Enter. This will install a tool called `yarn`, which we'll use to install dependencies and run the server.
 6. In your terminal, type `yarn`, and press Enter. This will install the latest dependencies you need to run the server. It may occasionally change the `yarn.lock` file, and that's ok.

## Install Github Desktop

Go to [desktop.github.com](https://desktop.github.com/) and download the latest github desktop for your computer.
This tool will make it easier for you to upload changes you make to the website without you having to know a lot of commands.

 1. Download the install Github Desktop
 2. Skip the sign in step, you won't need any github credentials, and you won't be asked again.
 3. Enter your name and laurier email address. This will not make any sort of account, this is used internally by git to identify who made a change to the website.
 4. Click "Add an Existing Repository from your hard drive ..."
 5. Click "Choose Path"
 6. Double-click on the "panda-lab" folder from your home directory/
 7. Click Ok/Select.
 8. Click "Add Repository"
 9. All done.
